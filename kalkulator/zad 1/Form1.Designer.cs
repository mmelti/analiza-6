﻿namespace zad_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btplus = new System.Windows.Forms.Button();
            this.btminus = new System.Windows.Forms.Button();
            this.btputa = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btsin = new System.Windows.Forms.Button();
            this.btcos = new System.Windows.Forms.Button();
            this.btpow = new System.Windows.Forms.Button();
            this.btsqrt = new System.Windows.Forms.Button();
            this.btlog = new System.Windows.Forms.Button();
            this.btjednako = new System.Windows.Forms.Button();
            this.txtRez = new System.Windows.Forms.TextBox();
            this.lb_rez = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btplus
            // 
            this.btplus.Location = new System.Drawing.Point(138, 108);
            this.btplus.Name = "btplus";
            this.btplus.Size = new System.Drawing.Size(35, 35);
            this.btplus.TabIndex = 0;
            this.btplus.Text = "+";
            this.btplus.UseVisualStyleBackColor = true;
            this.btplus.Click += new System.EventHandler(this.operator_click);
            // 
            // btminus
            // 
            this.btminus.Location = new System.Drawing.Point(179, 108);
            this.btminus.Name = "btminus";
            this.btminus.Size = new System.Drawing.Size(35, 35);
            this.btminus.TabIndex = 1;
            this.btminus.Text = "-";
            this.btminus.UseVisualStyleBackColor = true;
            this.btminus.Click += new System.EventHandler(this.operator_click);
            // 
            // btputa
            // 
            this.btputa.Location = new System.Drawing.Point(220, 108);
            this.btputa.Name = "btputa";
            this.btputa.Size = new System.Drawing.Size(35, 35);
            this.btputa.TabIndex = 2;
            this.btputa.Text = "*";
            this.btputa.UseVisualStyleBackColor = true;
            this.btputa.Click += new System.EventHandler(this.operator_click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(138, 153);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(35, 35);
            this.button3.TabIndex = 3;
            this.button3.Text = "/";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.operator_click);
            // 
            // btsin
            // 
            this.btsin.Location = new System.Drawing.Point(179, 153);
            this.btsin.Name = "btsin";
            this.btsin.Size = new System.Drawing.Size(35, 35);
            this.btsin.TabIndex = 4;
            this.btsin.Text = "sin(";
            this.btsin.UseVisualStyleBackColor = true;
            this.btsin.Click += new System.EventHandler(this.operator_click);
            // 
            // btcos
            // 
            this.btcos.Location = new System.Drawing.Point(220, 153);
            this.btcos.Name = "btcos";
            this.btcos.Size = new System.Drawing.Size(35, 35);
            this.btcos.TabIndex = 5;
            this.btcos.Text = "cos(";
            this.btcos.UseVisualStyleBackColor = true;
            this.btcos.Click += new System.EventHandler(this.operator_click);
            // 
            // btpow
            // 
            this.btpow.Location = new System.Drawing.Point(220, 198);
            this.btpow.Name = "btpow";
            this.btpow.Size = new System.Drawing.Size(35, 35);
            this.btpow.TabIndex = 8;
            this.btpow.Text = "pow(";
            this.btpow.UseVisualStyleBackColor = true;
            this.btpow.Click += new System.EventHandler(this.operator_click);
            // 
            // btsqrt
            // 
            this.btsqrt.Location = new System.Drawing.Point(179, 198);
            this.btsqrt.Name = "btsqrt";
            this.btsqrt.Size = new System.Drawing.Size(35, 35);
            this.btsqrt.TabIndex = 7;
            this.btsqrt.Text = "sqrt(";
            this.btsqrt.UseVisualStyleBackColor = true;
            this.btsqrt.Click += new System.EventHandler(this.operator_click);
            // 
            // btlog
            // 
            this.btlog.Location = new System.Drawing.Point(138, 198);
            this.btlog.Name = "btlog";
            this.btlog.Size = new System.Drawing.Size(35, 35);
            this.btlog.TabIndex = 6;
            this.btlog.Text = "log(";
            this.btlog.UseVisualStyleBackColor = true;
            this.btlog.Click += new System.EventHandler(this.operator_click);
            // 
            // btjednako
            // 
            this.btjednako.Location = new System.Drawing.Point(266, 153);
            this.btjednako.Name = "btjednako";
            this.btjednako.Size = new System.Drawing.Size(35, 78);
            this.btjednako.TabIndex = 9;
            this.btjednako.Text = "=";
            this.btjednako.UseVisualStyleBackColor = true;
            this.btjednako.Click += new System.EventHandler(this.btjednako_Click);
            // 
            // txtRez
            // 
            this.txtRez.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtRez.Location = new System.Drawing.Point(12, 59);
            this.txtRez.Name = "txtRez";
            this.txtRez.Size = new System.Drawing.Size(249, 22);
            this.txtRez.TabIndex = 10;
            this.txtRez.Text = "0";
            this.txtRez.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lb_rez
            // 
            this.lb_rez.AutoSize = true;
            this.lb_rez.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_rez.Location = new System.Drawing.Point(12, 26);
            this.lb_rez.Name = "lb_rez";
            this.lb_rez.Size = new System.Drawing.Size(0, 16);
            this.lb_rez.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(97, 198);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(35, 35);
            this.button1.TabIndex = 20;
            this.button1.Text = "3";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btn_click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(56, 198);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(35, 35);
            this.button2.TabIndex = 19;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btn_click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(15, 198);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(35, 35);
            this.button4.TabIndex = 18;
            this.button4.Text = "1";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.btn_click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(97, 153);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(35, 35);
            this.button5.TabIndex = 17;
            this.button5.Text = "6";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.btn_click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(56, 153);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(35, 35);
            this.button6.TabIndex = 16;
            this.button6.Text = "5";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.btn_click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(15, 153);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(35, 35);
            this.button7.TabIndex = 15;
            this.button7.Text = "4";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.btn_click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(97, 108);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(35, 35);
            this.button8.TabIndex = 14;
            this.button8.Text = "9";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.btn_click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(56, 108);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(35, 35);
            this.button9.TabIndex = 13;
            this.button9.Text = "8";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.btn_click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(15, 108);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(35, 35);
            this.button10.TabIndex = 12;
            this.button10.Text = "7";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.btn_click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(15, 243);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(76, 35);
            this.button11.TabIndex = 21;
            this.button11.Text = "0";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.btn_click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 282);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.lb_rez);
            this.Controls.Add(this.txtRez);
            this.Controls.Add(this.btjednako);
            this.Controls.Add(this.btpow);
            this.Controls.Add(this.btsqrt);
            this.Controls.Add(this.btlog);
            this.Controls.Add(this.btcos);
            this.Controls.Add(this.btsin);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btputa);
            this.Controls.Add(this.btminus);
            this.Controls.Add(this.btplus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Kalkulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btplus;
        private System.Windows.Forms.Button btminus;
        private System.Windows.Forms.Button btputa;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btsin;
        private System.Windows.Forms.Button btcos;
        private System.Windows.Forms.Button btpow;
        private System.Windows.Forms.Button btsqrt;
        private System.Windows.Forms.Button btlog;
        private System.Windows.Forms.Button btjednako;
        private System.Windows.Forms.TextBox txtRez;
        private System.Windows.Forms.Label lb_rez;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
    }
}

