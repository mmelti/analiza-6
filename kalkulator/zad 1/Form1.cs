﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zad_1
{
    public partial class Form1 : Form
    {
        double rezultat = 0;
        string izvedenaop = "";
        bool isOperationPerformed = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btn_click(object sender, EventArgs e)
        {
            if (txtRez.Text == "0" || isOperationPerformed)
                txtRez.Clear();
            isOperationPerformed = false;
            Button button= (Button)sender;
            txtRez.Text = txtRez.Text + button.Text;
        }

        private void operator_click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            izvedenaop = button.Text;
            rezultat = double.Parse(txtRez.Text);
            lb_rez.Text = rezultat + " " + izvedenaop;
            isOperationPerformed = true;
        }

        private void btjednako_Click(object sender, EventArgs e)
        {
            switch (izvedenaop)
            {
                case "+":
                    txtRez.Text = (rezultat + double.Parse(txtRez.Text)).ToString();
                    break;
                case "-":
                    txtRez.Text = (rezultat - double.Parse(txtRez.Text)).ToString();
                    break;
                case "*":
                    txtRez.Text = (rezultat * double.Parse(txtRez.Text)).ToString();
                    break;
                case "/":
                    txtRez.Text = (rezultat / double.Parse(txtRez.Text)).ToString();
                    break;
                case "sin(":
                    txtRez.Text = (Math.Sin( double.Parse(txtRez.Text))).ToString();
                    break;
                case "cos(":
                    txtRez.Text = (Math.Cos(double.Parse(txtRez.Text))).ToString();
                    break;
                case "log(":
                    txtRez.Text = (Math.Log10(double.Parse(txtRez.Text))).ToString();
                    break;
                case "sqrt(":
                    txtRez.Text = (Math.Sqrt(double.Parse(txtRez.Text))).ToString();
                    break;
                case "pow(":
                    txtRez.Text = (Math.Pow(double.Parse(txtRez.Text),2)).ToString();
                    break;
                default:
                    break;
            }
        }

        private void btnapredno(object sender, EventArgs e)
        {

        }
    }
}
